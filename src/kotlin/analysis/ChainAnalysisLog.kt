package analysis

import provideLineSeparator

/**
 * Represents chain analysis log
 *
 * @author tujhex
 * @since 15.11.2017
 **/
data class ChainAnalysisLog(private val log: String) {
    /**
     * Returns analysis log lines
     *
     * @return log lines
     */
    fun getLogLines(): List<String> {
        return log.split(provideLineSeparator())
    }

    override fun toString(): String {
        return log
    }
}