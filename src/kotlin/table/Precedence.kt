package table

/**
 * @author tujhex
 * @since 15.11.2017
 **/
enum class Precedence(private val value: Char) {
    NONE('N'),
    WEAK('W'),
    SIMPLE('S');

    override fun toString(): String {
        return value.toString()
    }
}