package core

import java.util.function.Consumer

/**
 * @author tujhex
 * @since 15.11.2017
 **/
class InputDataParser {
    /**
     * Returns parsed rules and chains
     *
     * @param inputData lines of data
     */
    fun parseInputData(inputData: List<String>): Pair<ArrayList<Rule>, ArrayList<Chain>> {
        val rules = ArrayList<Rule>()
        val chains = ArrayList<Chain>()

        val ruleConsumer = Consumer<String> { rules.add(parseRule(it)) }
        val chainConsumer = Consumer<String> { chains.add(Chain(it)) }
        var lineObserver: Consumer<String>
        lineObserver = ruleConsumer

        // read and parse input data
        inputData
            .forEach {
                if (it.isEmpty()) {
                    lineObserver = chainConsumer
                    return@forEach
                }
                lineObserver.accept(it)
            }
        return Pair(rules, chains)
    }
}