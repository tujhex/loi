/**
 * @author tujhex
 * @since 16.11.2017
 **/

fun provideLineSeparator():String{
    return System.lineSeparator()
}