import analysis.ChainAnalysisLog
import analysis.ChainAnalyzer
import analysis.provideErrorMessage
import core.InputDataParser
import core.isArgumentsCorrect
import table.PrecedenceTableParser
import java.io.File
import table.Precedence

/**
 * @author tujhex
 * @since 12.11.2017
 **/
fun main(args: Array<String>) {
    if (!isArgumentsCorrect(args)) {
        println("Неправильные аргументы; Нужно указать входной и выходной файлы")
        return
    }

    val (rules, chains) = InputDataParser().parseInputData(File(args[0]).readLines())
    val parser = PrecedenceTableParser()
    val table = parser.parseTable(rules)
    val analyzer = ChainAnalyzer()
    val axiom = rules.first().nonTerminal
    val analysis: ArrayList<ChainAnalysisLog> = ArrayList()
    val tableType = parser.getTableType(table, rules)
    if (tableType != Precedence.NONE) {
        chains.forEach { chain ->
            analysis.add(analyzer.analyzeChain(chain, table, rules, axiom))
        }
    }
    File(args[1]).printWriter().use { out ->
        out.write(table.toString())
        out.write(provideLineSeparator())
        out.write(tableType.toString())
        out.write(provideLineSeparator())
        analysis.forEach {
            out.write(provideLineSeparator())
            out.write(it.toString())
            val last = it.getLogLines().last()
            if (last != provideErrorMessage()) {
                out.write(provideLineSeparator())
                out.write(last.replace("\\s".toRegex(), ""))
            }
            out.write(provideLineSeparator())
        }
    }
}