package table

import core.Rule

/**
 * @author tujhex
 * @since 13.11.2017
 **/
class PrecedenceTableParser {
    private var first: HashMap<Char, HashSet<Char>> = HashMap()
    private var last: HashMap<Char, HashSet<Char>> = HashMap()
    private var head: HashMap<Char, HashSet<Char>> = HashMap()

    /**
     * Return Precedence type from PrecedenceTable and grammar rules
     *
     * @param table precedence table
     * @param rules grammar rules
     * @return precedence
     */
    fun getTableType(table: PrecedenceTable, rules: ArrayList<Rule>): Precedence {
        var size = 0
//       <, >, =, <= fits
        var isFitRelationRule = true
        table.rows.forEach { row ->
            row.value.forEach row@ { col ->
                val value = col.value
                size = maxOf(value.size, size)
                if (value.size <= 1) {
                    return@row
                }
                if ((value.contains(Relation.LESS) && value.contains(Relation.MORE))
                    or (value.contains(Relation.MORE) && value.contains(Relation.EQUAL))) {
                    isFitRelationRule = false
                }

            }
        }
        if (size == 1) {
            return Precedence.SIMPLE
        }
        var isFitRule = true
        val pairs: ArrayList<Pair<Char, Char>> = ArrayList()
        rules.forEach { currentRule ->
            currentRule.production.forEach char@ { char ->
                if (char.isTerminal()) {
                    return@char
                }
                rules.forEach rule@ { rule ->
                    if (rule.nonTerminal == char || rule == currentRule) {
                        return@rule
                    }
                    val index = currentRule.production.indexOf(rule.nonTerminal)
                    if (rule.production == currentRule.production.substring(index + 1)) {
                        pairs.add(Pair(char, rule.nonTerminal))
                    }
                }
            }
        }
        pairs.forEach { (first, second) ->
            table.rows[first]?.let { col ->
                col[second]?.let { value ->
                    if (value.size == 1 && (value.contains(Relation.LESS) || value.contains(Relation.EQUAL))) {
                        isFitRule = false
                    }
                }
            }
        }
        if (isFitRule && isFitRelationRule) {
            return Precedence.WEAK
        }
        return Precedence.NONE
    }

    /**
     * Добивает значения нетерминалов в списки
     */
    private fun appendValues(list: HashMap<Char, HashSet<Char>>): HashMap<Char, HashSet<Char>> {
        val newList: HashMap<Char, HashSet<Char>> = HashMap()
        var temp = list
        temp.forEach { key, value ->
            newList.put(key, HashSet(value))
        }
        for (i in 0..list.size) {
            temp.forEach { key, set ->
                set.forEach { char ->
                    if (char.isNonTerminal()) {
                        temp[char]?.let { value ->
                            newList[key]?.addAll(value)
                        }
                    }
                }
            }
            temp = HashMap()
            newList.forEach { key, value ->
                temp.put(key, HashSet(value))
            }
        }

        return newList
    }

    /**
     * Creates PrecedenceTable based on grammar rules
     *
     * For detail google 'Wirth–Weber precedence relationship'
     *
     * @param rules grammar rules
     * @return precedence table
     */
    fun parseTable(rules: ArrayList<Rule>): PrecedenceTable {
        val axiom = rules.first().nonTerminal
        rules.forEach {
            if (!first.containsKey(it.nonTerminal)) {
                first.put(it.nonTerminal, HashSet())
                last.put(it.nonTerminal, HashSet())
            }
            first[it.nonTerminal]?.add(it.production.first())
            last[it.nonTerminal]?.add(it.production.last())
        }
        last = appendValues(last)
        first = appendValues(first)
        val symbols: ArrayList<Char> = ArrayList()
        rules.forEach {
            it.production.forEach { key ->
                if (!symbols.contains(key)) {
                    symbols.add(key)
                }
                if (!head.containsKey(key)) {
                    head.put(key, HashSet())
                    if (key.isTerminal()) {
                        head[key]?.add(key)
                    } else {
                        first[key]?.let { value ->
                            value.forEach {
                                if (it.isTerminal()) {
                                    head[key]?.add(it)
                                }
                            }
                        }
                    }
                }
            }
        }

        val comparator: Comparator<Char> = Comparator { left, right ->
            when {
                left == axiom -> -1
                right == axiom -> 1
                left.isTerminal() && right.isNonTerminal() -> 1
                left.isNonTerminal() && right.isTerminal() -> -1
                else -> left.compareTo(right)
            }
        }
        symbols.sortWith(comparator)

        val table = PrecedenceTable(symbols)

        rules.forEach { rule ->
            rule.production.forEachIndexed { index, char ->
                if (index < rule.production.length - 1) {
                    val nextChar = rule.production[index + 1]
                    table.addRelation(char, nextChar, Relation.EQUAL)
                    first[nextChar]?.let { set -> set.forEach { table.addRelation(char, it, Relation.LESS) } }
                    last[char]?.forEach { x ->
                        head[nextChar]?.forEach { y ->
                            table.addRelation(x, y, Relation.MORE)
                        }
                    }
                }
            }
        }
        first[axiom]?.forEach {
            table.addRelation(provideRowStart(), it, Relation.LESS)
        }
        last[axiom]?.forEach {
            table.addRelation(it, provideRowEnd(), Relation.MORE)
        }
        table.addRelation(axiom, provideRowEnd(), Relation.MORE)
        table.addRelation(provideRowStart(), axiom, Relation.LESS)

        table.rows.forEach { row ->
            for (col in row.value) {
                var newValue: HashSet<Relation> = col.value
                if (newValue.isEmpty()) {
                    newValue = HashSet()
                    newValue.add(Relation.NONE)
                }
                col.setValue(newValue)
            }
        }
        return table
    }
}