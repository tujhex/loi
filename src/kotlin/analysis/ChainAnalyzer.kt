package analysis

import core.Chain
import core.Rule
import provideLineSeparator
import table.PrecedenceTable
import table.Relation
import table.provideRowEnd
import table.provideRowStart

/**
 * @author tujhex
 * @since 15.11.2017
 **/
class ChainAnalyzer {
    /**
     * Analyzes chain by precedence table, grammar rules and axiom
     *
     * @param chain chain to analyze
     * @param table precedence table
     * @param rules grammar rules
     * @param axiom axiom of grammar
     * @return chain analysis log
     */
    fun analyzeChain(chain: Chain, table: PrecedenceTable, rules: ArrayList<Rule>, axiom: Char): ChainAnalysisLog {
        val stopRuleStack = "${provideRowStart()}$axiom"
        var stack = provideRowStart().toString()
        val mappedRules: HashMap<String, Char> = HashMap()
        rules.forEach {
            mappedRules.put(it.production, it.nonTerminal)
        }
        var input = chain.chain + provideRowEnd()
        var result = "$stack $input"
        while (stack != stopRuleStack || input != provideRowEnd().toString()) {
            val stackTop = stack.last()
            val inputTop = input.first()
            val relations = table.getRelation(stackTop, inputTop)
            if (relations.contains(Relation.NONE)) {
                result += "${provideLineSeparator()}${provideErrorMessage()}"
                return ChainAnalysisLog(result)
            }
            if (!relations.contains(Relation.MORE)) {
                stack += inputTop
                input = input.substring(1, input.length)
                result += "${provideLineSeparator()}$stack $input"
                continue
            }
            var rule = ""
            var count = 0

            stack.reversed().forEach lit@{
                rule = it + rule
                count++
                if (mappedRules.containsKey(rule)) {
                    stack = stack.substring(0, stack.length - count) + mappedRules[rule].toString()
                    return@lit
                }
            }
            if (stack == rule){
                result += "${provideLineSeparator()}${provideErrorMessage()}"
                return ChainAnalysisLog(result)
            }
            result += "${provideLineSeparator()}$stack $input"
        }
        return ChainAnalysisLog(result)
    }
}