package table

import provideLineSeparator

/**
 * Precedence table for formal grammar
 * @author tujhex
 * @since 13.11.2017
 **/
data class PrecedenceTable(private val symbols: List<Char>) {
    internal var rows: HashMap<Char, HashMap<Char, HashSet<Relation>>> = HashMap()
    private var stringRepresentation = ""

    /**
     * Adds start and end symbols to table
     */
    init {
        symbols.forEach { sym1 ->
            val col: HashMap<Char, HashSet<Relation>> = HashMap()
            symbols.forEach { sym2 ->
                col.put(sym2, HashSet())
            }
            col.put(provideRowEnd(), HashSet())
            rows.put(sym1, col)
        }
        val col: HashMap<Char, HashSet<Relation>> = HashMap()
        symbols.forEach { sym -> col.put(sym, HashSet()) }
        col.put(provideRowEnd(), HashSet())
        rows.put(provideRowStart(), col)
    }

    /**
     * Adds relation between chars in table
     *
     * @param rowKey char in row
     * @param colKey char in column
     * @param relation relation between chars
     */
    fun addRelation(rowKey: Char, colKey: Char, relation: Relation) {
        rows[rowKey]?.let { col ->
            col[colKey]?.let { value ->
                if (!value.contains(relation)) {
                    value.add(relation)
                }
            }
        }
    }

    /**
     * Returns relation between chars in table
     *
     * @param first char in row
     * @param second char in column
     * @return set of relations between chars
     */
    fun getRelation(first: Char, second: Char): HashSet<Relation> {
        rows[first]?.let { col -> col[second]?.let { return it } }
        return hashSetOf(Relation.NONE)
    }

    /**
     * Overridden to provide table-like representation of table
     */
    override fun toString(): String {
        if (stringRepresentation.isNotEmpty()) {
            return stringRepresentation
        }
        var result = "  "
        val sizes: HashMap<Char, Int> = HashMap()
        val symbolsWithEnd = symbols + provideRowEnd()
        val symbolsWithStart = symbols + provideRowStart()
        for (key in symbolsWithEnd) {
            rows[key]?.let { col ->
                for (key2 in symbolsWithStart) {
                    sizes[key2]?.let { stored ->
                        col[key2]?.let {
                            sizes.put(key2, maxOf(stored, it.size))
                        }
                    } ?: let {
                        col[key2]?.let {
                            sizes.put(key2, it.size)
                        }
                    }
                }
            }
        }

        for (key in symbolsWithEnd) {
            result += "$key"
            sizes[key]?.let {
                for (i in 1..it) {
                    result += " "
                }
            }
        }
        val comparator: Comparator<Relation> = Comparator { left, right ->
            left.ordinal.compareTo(right.ordinal)
        }
        result += provideLineSeparator()
        for (key in symbolsWithStart) {
            result += "$key "
            rows[key]?.let { col ->
                for (key2 in symbolsWithEnd) {
                    col[key2]?.sortedWith(comparator)?.forEach { relation ->
                        result += "$relation"
                    }
                    var size = 0
                    col[key2]?.let {
                        size = it.size - 1
                    }
                    sizes[key2]?.let {
                        for (i in 1..(it - size)) {
                            result += " "
                        }
                    }
                }
            }
            result += provideLineSeparator()
        }
        stringRepresentation = result.substring(0, result.length - provideLineSeparator().length)
        return stringRepresentation
    }
}