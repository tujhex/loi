package core

/**
 * Rule representation
 *
 * @author tujhex
 * @since 13.11.2017
 **/
data class Rule(val nonTerminal: Char, val production: String)