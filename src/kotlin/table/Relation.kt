package table

/**
 * @author tujhex
 * @since 15.11.2017
 **/
enum class Relation(private val value: Char) {
    NONE('.'),
    LESS('<'),
    MORE('>'),
    EQUAL('=');

    override fun toString(): String {
        return value.toString()
    }
}