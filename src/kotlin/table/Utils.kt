package table

/**
 * @author tujhex
 * @since 15.11.2017
 **/
fun Char.isTerminal(): Boolean = this.isDigit() || (this.isLetter() && this.isLowerCase())

fun Char.isNonTerminal(): Boolean = this.isLetter() && this.isUpperCase()

fun provideRowEnd(): Char = '$'
fun provideRowStart(): Char = '^'
