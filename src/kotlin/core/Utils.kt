package core

import java.io.File

/**
 * @author tujhex
 * @since 13.11.2017
 **/
fun parseRule(input: String): Rule {
    val params = input.split(delimiters = " ", ignoreCase = true, limit = 2)
    return Rule(params[0][0], params[1])
}

fun isArgumentsCorrect(args: Array<String>): Boolean {
    if (args.size != 2) {
        return false
    }
    val input = File(args[0])
    if (!input.exists() && !input.canRead() && !input.isFile) {
        return false
    }
    return true
}