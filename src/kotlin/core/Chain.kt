package core

/**
 * Chain representation
 *
 * @author tujhex
 * @since 13.11.2017
 **/
data class Chain(val chain: String)